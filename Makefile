# This is a Makefile to save the commands for ROS_BAG
file_path = $(path)
topic_name = $(topic)

record_all:
	rosbag record -a --duration=2m -o TEST

record_states:
	rosbag record --duration=1m -o bag_files/SIM_MODEL /gazebo/model_states

record_simulation:
	rosbag record --duration=90s -o bag_files/SIM_MODEL /gazebo/model_states /inferred_pose

record_cmd:
	rosbag record --duration=90s -o bag_files/SIM_CMD /brainiac/mobile_base_controller/cmd_vel

bag_info:
	rosbag info -y $(file_path)

bag_play:
	rosbag play $(file_path) --topics /brainiac/mobile_base_controller/cmd_vel

topic_msg_size:
	rostopic echo -n1 topic_name | wc -c