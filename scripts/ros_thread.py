import os
import logging
import threading
import subprocess

ROOT_PATH = os.path.dirname(os.getcwd())
BAG_PLAY = os.path.join(ROOT_PATH, "bag_files", "SIM_CMD_2021-04-21-14-29-07.bag")


def thread_function_play():
    logging.info("Thread: BAG_PLAY -- START")
    subprocess.Popen(['rosbag', 'play', BAG_PLAY, '--topics', '/brainiac/mobile_base_controller/cmd_vel'],
                     stdout=subprocess.PIPE)
    logging.info("Thread: BAG_PLAY -- FINISH")


def thread_function_record():
    logging.info("Thread: Recording Bag file -- START")
    subprocess.Popen(['rosbag', 'record', '--duration=60s', '-o',
                      os.path.join(ROOT_PATH, 'bag_files', 'SIM_MODEL'),
                      '/gazebo/model_states', '/inferred_pose'],
                     stdout=subprocess.PIPE)
    logging.info("Thread: Recording Bag file -- FINISH")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        datefmt="%H:%M:%S")
    threads = list()
    function_list = [thread_function_play, thread_function_record]
    for index, function_call in enumerate(function_list):
        logging.info("Main: create and start thread %d.", index)
        thread_var = threading.Thread(target=function_call)
        threads.append(thread_var)
        thread_var.start()

    for index, thread in enumerate(threads):
        logging.info("Main: before joining thread %d.", index)
        thread.join()
        logging.info("Main: thread %d done", index)
