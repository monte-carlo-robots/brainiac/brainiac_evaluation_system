import os
import yaml
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas.io.json import json_normalize

ROOT_PATH = os.path.dirname(os.path.dirname(__file__))


def consolidate_position_gazebo(column_series):
    """
    This function returns the column series with yaml data parsed and filtered
    :param column_series: pandas column series
    :return: column series with yaml data parsed and filtered
    """
    yaml_data = yaml.load(column_series)
    brainiac_data_index = yaml_data['name'].index('brainiac')
    return yaml_data['pose'][brainiac_data_index]['position']


def consolidate_position_pose(column_series):
    """
    This function returns the column series with yaml data parsed on position
    :param column_series: pandas column series
    :return: column series with yaml data parsed on position
    """
    yaml_data = yaml.load(column_series)
    return yaml_data['pose']['pose']['position']


def dataframe_name_extension(dataframe, column, prefix):
    """
    This function explodes nested columns and adds a prefix to the column name
    :param dataframe: pandas dataframe
    :param column: pandas column
    :param prefix: prefix to add to the column name
    :return: dataframe adding the renamed columns with the prefix
    """
    dataframe_norm = json_normalize(dataframe[column])
    dataframe_norm.index = dataframe.index
    dataframe_norm.columns = prefix + dataframe_norm.columns
    return dataframe.join(dataframe_norm)


def explode_columns(dataframe):
    """
    This function explodes nested columns on the [msg_x, msg_y] columns
    :param dataframe: pandas dataframe
    :return: return pandas dataframe with nested columns exploded on the [msg_x, msg_y] columns
    """
    dataframe['msg_x'] = dataframe['msg_x'].apply(consolidate_position_gazebo)
    dataframe['msg_y'] = dataframe['msg_y'].apply(consolidate_position_pose)
    dataframe = dataframe_name_extension(dataframe, 'msg_x', 'model_state_')
    dataframe = dataframe_name_extension(dataframe, 'msg_y', 'model_poses_')
    return dataframe


def process_position_file(arg):
    """
    This function is thw workflow for processing csv position file from topic data
    :param arg: arguments from argument parser
    :return: pandas processed dataframe
    """
    raw_csv_file = arg.position_file
    dataset = pd.read_csv(raw_csv_file, sep=',', header=0)
    dataset.time = (dataset.time * 10e-8).apply(np.ceil)
    dataset_filter = dataset[dataset.topic != '/gazebo/model_states']
    dataset = dataset.merge(dataset_filter, on='time', how='inner')
    dataset_prep = dataset.drop_duplicates(subset=['topic_x', 'topic_y', 'time'], keep='first')
    dataset_prep = dataset_prep[dataset_prep.topic_x != '/inferred_pose']
    dataset_out = explode_columns(dataset_prep)
    dataset_out = dataset_out.reset_index(drop=True)
    dataset_out['particle_index'] = dataset_out.index
    dataset_out.to_csv(arg.out)
    return dataset_out


def create_graph_scatter(dataset, resolution, title, img_path=None):
    """
    This function generates scatter plot comparing positions from the provided dataset
    ( [model_state_x, model_state_y] with [model_poses_x, model_poses_y] )
    :param dataset: pandas dataset with position data
    :param img_path: img path output to save the plot
    :param title: title to label the experiment
    :param resolution: resolution parameter on the graph
    """
    if img_path is None:
        img_path = os.path.join(ROOT_PATH, 'images', 'plot_scatter')
    fig, ax = plt.subplots(figsize=(10, 10))
    model_state = ax.scatter(dataset['model_state_x'], dataset['model_state_y'], color='Black')
    model_poses = ax.scatter(dataset['model_poses_x'], dataset['model_poses_y'], color='Red')
    for k, v in dataset[['model_state_x', 'model_state_y']].iterrows():
        random_bias = tuple(np.random.randint(-10, 10, size=1) + np.array([10, 5]))
        ax.annotate(k, v, xytext=random_bias, textcoords='offset points', size=12, color='Black')
    image_bias = 0
    for k, v in dataset[['model_poses_x', 'model_poses_y']].iterrows():
        random_bias = tuple(np.random.randint(-10, 10, size=1) + np.array([10, 5]))
        ax.annotate(k, v, xytext=random_bias, textcoords='offset points', size=12, color='Red')
        image_bias += 4
    # Adding legend
    plt.title("Scatter plot of real and estimated pose from Particle Filter: " + title)
    plt.legend((model_state, model_poses), ('Real Pose', 'Estimated Pose'), scatterpoints=1,
               loc='upper right', ncol=3, fontsize=12)
    # Adding axis map limit and bins
    plt.xlim(-0.5, 0.5)
    plt.ylim(-0.5, 0.5)
    plt.xticks(np.arange(-0.5, 0.5, step=float(resolution)))
    plt.yticks(np.arange(-0.5, 0.5, step=float(resolution)))
    # Saving map
    plt.grid()
    plt.savefig(img_path)


def euclidean_dist(dataframe):
    """
    This function returns a column with euclidean distances from the columns
    ( [model_state_x, model_state_y] ;  [model_poses_x, model_poses_y] )
    :param dataframe: pandas dataset with position data
    :return: pandas column series with euclidean distances from the columns described above
    """
    cols = ['model_state_x', 'model_state_y']
    cols2 = ['model_poses_x', 'model_poses_y']
    return np.linalg.norm(dataframe[cols].values - dataframe[cols2].values, axis=1)


def create_graph_distance(dataframe, resolution, title, img_path=None):
    """
    This function generates x-y plot comparing euclidean distances from the provided dataset
    ( [model_state_x, model_state_y] with [model_poses_x, model_poses_y] )
    :param dataframe: pandas dataset with position data
    :param img_path: img path output to save the plot
    :param title: title to label the experiment
    :param resolution: resolution parameter on the graph
    """
    if img_path is None:
        img_path = os.path.join(ROOT_PATH, 'images', 'plot_euclidean')
    fig, ax = plt.subplots(figsize=(10, 10))
    dataframe['Euclidean Distance'] = euclidean_dist(dataframe)
    dataframe.plot(x='particle_index', y='Euclidean Distance', kind='line', ax=ax)
    plt.title("Euclidean plot of real and estimated pose from Particle Filter: " + title)
    plt.yticks(np.arange(0, 1, step=float(resolution)))
    plt.ylim(0, 1)
    plt.grid()
    plt.savefig(img_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='MCL analysis script')
    # Add the arguments
    parser.add_argument('--position-file', metavar='csv_file',
                        type=str, action="store",
                        help='Path to csv position input file')
    parser.add_argument('--out', metavar='output',
                        type=str, action="store",
                        default='position_analysis.csv', help='Path of the output position processed csv file')
    parser.add_argument("--graph", help="Enable creating graphs",
                        type=bool,
                        action="store")
    parser.add_argument("--resolution", help="Map resolution to plot", default='0.05',
                        type=str, action="store")
    parser.add_argument("--title", help="Title for graph",
                        default='exp 1',
                        type=str, action="store")
    args = parser.parse_args()
    dataframe_result = process_position_file(args)
    if args.graph:
        create_graph_scatter(dataframe_result, args.resolution, args.title)
        create_graph_distance(dataframe_result, args.resolution, args.title)
