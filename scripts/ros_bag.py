import os
import csv
import argparse
import rosbag
import pprint
import subprocess

ROOT_PATH = os.path.dirname(os.getcwd())


def show_topics_types(rosbag_path):
    """
    This function prints on the terminal the rosbag topics types and metadata
    :param rosbag_path: path to the bag file
    """
    pprint_instance = pprint.PrettyPrinter(depth=4)
    bag_handler = rosbag.Bag(rosbag_path)
    topics_dict = bag_handler.get_type_and_topic_info()[1]
    topics = topics_dict.keys()
    types = [topics_dict[i][0] for i in topics]
    bag_handler.close()
    pprint_instance.pprint(types)


def export_bag_info_to_yaml(bag_file):
    """
    This function exports to a yaml file the rosbag topics types and metadata
    The file is named with the same main name from the bag file and is placed in the data_files directory
    :param bag_file: path to the bag file
    """
    filename = '{}'.format(bag_file.replace('bag_files', 'data_files').replace('.bag', '.yaml'))
    with open(filename, 'w+') as file_descriptor:
        file_descriptor.write(subprocess.Popen(['rosbag', 'info', '--yaml', bag_file],
                                               stdout=subprocess.PIPE).communicate()[0])


def export_topics_data_to_csv(bag, topics, file_path=None):
    """
    This function exports the topics msg data to a csv file to future analysis
    :param bag: instance of the bag file descriptor that handles bag objects
    :param topics: list of topics that must be on the output csv file
    :param file_path: name of the output csv file
    """
    if file_path is None:
        file_path = os.path.join(ROOT_PATH, 'data_files', 'topic_raw_data')
    with open('{0}.csv'.format(file_path), mode='w') as data_file:
        try:
            data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            data_writer.writerow(['topic', 'time', 'msg'])
            for topic, msg, time in bag.read_messages(topics=['{}'.format(topic) for topic in topics]):
                data_writer.writerow([topic, time, msg])
        except ValueError:
            print('This file does not contain one of the topics passed as argument')


def process_rosbag(arg_parser):
    """
    This function is the main workflow to process the bag file passed as an argument
    :param arg_parser: instance of the argument parser containing all the arguments from the file call
    """
    rosbag_path = arg_parser.rosbag
    topics_list = arg_parser.nargs
    bag_handler = rosbag.Bag(rosbag_path)
    export_topics_data_to_csv(bag_handler, topics_list, file_path=arg_parser.out)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='MCL rosbag process')
    # Add the arguments
    parser.add_argument('--rosbag', type=str, help='Path to bag file')
    parser.add_argument('--nargs', nargs='+', help='Topics list that must be on the output csv file')
    parser.add_argument('--out', metavar='output',
                        type=str, action="store",
                        default='topic_raw_data', help='Output csv file path')
    # Execute the parse_args() method
    parser.add_argument("--rosbag-info", help="Exports yaml file with bag info",
                        type=str,
                        action="store")
    parser.add_argument("--rosbag-type", help="Shows message types from bag file",
                        type=str,
                        action="store")
    args = parser.parse_args()
    process_rosbag(args)
    if args.rosbag_info:
        export_bag_info_to_yaml(args.rosbag_info)
    if args.rosbag_type:
        show_topics_types(args.rosbag_type)
